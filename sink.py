# audiocom library: Source and sink functions
import common_srcsink as common
import Image
from graphs import *
import binascii
import random
from source import Source
import copy
import ast

class Sink:
    def __init__(self, compression):
        self.compression = compression
        print 'Sink:'

    def process(self, recd_bits):
        # Process the recd_bits to form the original transmitted
        # file. 
        # Here recd_bits is the array of bits that was 
        # passed on from the receiver. You can assume, that this 
        # array starts with the header bits (the preamble has 
        # been detected and removed). However, the length of 
        # this array could be arbitrary. Make sure you truncate 
        # it (based on the payload length as mentioned in 
        # header) before converting into a file.
        
        # If its an image, save it as "rcd-image.png"
        # If its a text, just print out the text
        recd_bits_list = [x for x in recd_bits]
        payloadtype, payloadlen, stats, message = self.read_header(recd_bits_list)
        
        if self.compression:
            srcbits = self.huffman_decode(message, stats)
        else:
            srcbits = message

        print '\tRecd', len(srcbits), 'source bits'

        if payloadtype == common.HEADER_IMAGE:
            self.image_from_bits(srcbits, "rcd-image.png")
        elif payloadtype == common.HEADER_TEXT:
            print self.bits2text(srcbits)
        else:
            print srcbits
    
        # Return the received source bits for comparison purposes
        return srcbits

    def bits2text(self, bits):
        # Convert the received payload to text (string)
        return common.bits_to_string(bits)
        
    def image_from_bits(self, bits, filename):
        # Convert the received payload to an image and save it
        # No return value required .
        newImage = Image.new("L", (32, 32))
        newImage.fromstring(common.bits_to_string(bits))
        newImage.save(filename, "png")
        pass

    def read_header(self, header_bits): 
        # Given the header bits, compute the payload length
        # and source type (compatible with get_header on source)
        # Get information for decompression if needed
        srctype = common.bits_to_int(header_bits[0:common.NUM_BITS_INT])
        stride = common.NUM_BITS_INT

        payload_length = common.bits_to_int(header_bits[stride:stride+common.NUM_BITS_INT])
        stride += common.NUM_BITS_INT

        if self.compression:
            statlen = common.bits_to_int(header_bits[stride:stride+common.NUM_BITS_INT])
            stride += common.NUM_BITS_INT
            statstr = common.bits_to_string(header_bits[stride:stride+statlen])
            stride += statlen
            stat = ast.literal_eval(statstr)
        else:
            stat = None
        payloadbits = header_bits[stride:stride+payload_length]
        if len(payloadbits) < payload_length:
            payloadbits.extend([0 for x in xrange(payload_length-len(payloadbits))])
        
        print '\tRecd header: ', header_bits[0:common.NUM_BITS_INT*2]
        print '\tLength from header: ', payload_length
        print '\tSource type: ', srctype
        return srctype, payload_length, stat, payloadbits

    #bit errors - something went wrong, kill the program
    def huffman_decode(self, codedbits, stat):
        # Given the source-coded bits and the statistics of symbols,
        # decompress the huffman code and return the decoded source bits
        sym = []
        srcbits = []

        longest_code_len = 0
        for code in stat.keys():
            longest_code_len = max(longest_code_len, len(code))

        for bits in codedbits:
            sym.append(bits)
            code = common.list_to_string(sym)
            if code in stat.keys():
                srcbits.extend(common.string_to_list(stat[code]))
                sym = []
            elif len(code) >= longest_code_len:
                print "Error reading bits. Stopping and Aborting"
                srcbits = []
                return srcbits
        # Another possible case: We didn't generate a code too long,
        # but we still weren't able to resolve it to a symbol
        if len(sym) > 0:
            print "Error reading bits. Stopping and Aborting"
            srcbits = []

        return srcbits

