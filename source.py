# audiocom library: Source and sink functions
import common_srcsink as common
import Image
from graphs import *
import binascii
import random
import copy

class Source:
    def __init__(self, monotone, filename=None, compress=False):
        self.monotone = monotone
        self.fname = filename
        self.compress = compress
        print 'Source: '

    def process(self):
            srcbits = []
            srctype = 0
            # Form the databits, from the filename
            if self.fname is not None:
                if self.fname.endswith('.png') or self.fname.endswith('.PNG'):
                    # Its an image
                    srcbits = self.bits_from_image(self.fname)
                    srctype = common.HEADER_IMAGE
                else:           
                    # assume it's text
                    srcbits = self.text2bits(self.fname)
                    srctype = common.HEADER_TEXT
            else:               
                # Send monotone (the bits are all 1s for monotone bits)
                srcbits = [1 for x in xrange(self.monotone)]
                srctype = common.HEADER_MONOTONE

            # Perform Huffman coding if the compression option is on
            # compress will be to be False if you send monotone
            # (handled in sendrecv.py)
            if self.compress:
                stats, payload = self.huffman_encode(srcbits)
            else:
                payload = srcbits
                stats = None

            header = self.get_header(len(payload), srctype, stats)
            databits = copy.deepcopy(header)
            databits.extend(payload)

            print '\tSource type: ', srctype
            print '\tPayload Length: ', len(payload)
            print '\tHeader: ', header[0:common.NUM_BITS_INT*2]

            # srcbits is the bit sequence representing source data
            # payload is the data part that is sent over the channel
            # databits is the bit sequence that is sent over the channel (including header)
            return srcbits, payload, databits

    def text2bits(self, filename):
        # Given a text file, convert to bits
        try:
            f = open(filename, 'r')
            data = f.read()
            bits = common.string_to_bits(data)
        except IOError:
            print "cannot open ", filename
            bits = []
        return bits

    def bits_from_image(self, filename):
        # Given an image, convert to bits
        try:
            # Open the image
            im = Image.open(filename,'r')
            # Convert to gray
            im = im.convert('L')
            bits = common.string_to_bits(im.tostring())
        except IOError:
            print "cannot open image ", filename
            bits = []
        return bits

    def get_header(self, payload_length, srctype, stat):
        # Given the payload length and the type of source 
        # (image, text, monotone), form the header
        # Add header-extension if needed

        header = common.int_to_bits(srctype) 
        header.extend(common.int_to_bits(payload_length))

        if stat is not None:
            huffman_table = common.string_to_bits(str(stat))
            huffman_length = common.int_to_bits(len(huffman_table))
            header.extend(huffman_length)
            header.extend(huffman_table)

        print '\tHeader length: ', len(header)

        return header

    def huffman_encode(self, srcbits):
        # Given the source bits, get the statistics of the symbols
        # Compress using huffman coding
        # Return statistics and the source-coded bits
        tree = common.HuffmanTree()
        stat, codedbits = tree.encode(srcbits)

        compression_rate = float(len(codedbits)) / float(len(srcbits))
        print "\tSource bit length: ", len(srcbits)
        print "\tSource-coded bit length: ", len(codedbits)
        print "\tCompression rate: ", compression_rate

        return stat, codedbits
