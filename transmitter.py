import math
import common_txrx as common
import numpy

import hamming_db
import channel_coding as cc
 
class Transmitter:
    def __init__(self, carrier_freq, samplerate, one, spb, silence, cc_len):
        self.fc = carrier_freq  # in cycles per sec, i.e., Hz
        self.samplerate = samplerate
        self.one = one
        self.spb = spb
        self.silence = silence
        self.cc_len = cc_len
        print 'Transmitter: '
        
    def add_preamble(self, databits):
        '''
        Prepend the array of source bits with silence bits and preamble bits
        The recommended preamble bits is 
        [1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1]
        The output should be the concatenation of arrays of
            [silence bits], [preamble bits], and [databits]
        '''
        zeros = numpy.tile([0], self.silence)
        #MVitelli - During testing, I found that my microphone would cut out early
        #and never read all of the transmitted samples, resulting in a truncated message.
        #Adding silence bits to the end of the message appeared to fix this problem
        databits_with_preamble = numpy.concatenate((zeros, common.preamble, databits, zeros))

        print '\tSent Preamble: ', common.preamble
        return databits_with_preamble

    def bits_to_samples(self, databits_with_preamble):
        '''
        Convert each bits into [spb] samples. 
        Sample values for bit '1', '0' should be [one], 0 respectively.
        Output should be an array of samples.
        '''
        samples = numpy.copy(databits_with_preamble)
        samples = samples.astype(float)
        samples[samples > 0] = self.one
        samples = numpy.repeat(samples, self.spb)
        return samples
        
    def modulate(self, samples):
        '''
        Multiply samples by a local sinusoid carrier of the same length.
        Return the multiplied result.
        '''
        carrier_freq = 2*numpy.pi*self.fc/self.samplerate
        cosine = numpy.cos(numpy.array(numpy.arange(0, samples.size)*carrier_freq))
        mod_samples = numpy.multiply(cosine, samples)
        mod_samples = common.lpfilter(mod_samples, 2*carrier_freq)
        print '\tNumber of samples being sent:', mod_samples.size
        return mod_samples
        
    def encode(self, databits, cc_len):
        '''
        Wrapper function for milestone 2. No need to touch it        
        '''
        return cc.get_frame(databits, cc_len)
