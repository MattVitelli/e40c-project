import sys
import math
import numpy as np
import matplotlib.pyplot as p
import scipy.cluster.vq
import common_txrx as common
from graphs import *
from numpy import linalg as LA
import copy

import hamming_db
import channel_coding as cc

class Receiver:
    def __init__(self, carrier_freq, samplerate, spb):
        '''
        The physical-layer receive function, which processes the
        received samples by detecting the preamble and then
        demodulating the samples from the start of the preamble 
        sequence. Returns the sequence of received bits (after
        demapping)
        '''
        self.fc = carrier_freq
        self.samplerate = samplerate
        self.spb = spb
        self.preamble_samples = common.get_preamble_samples(spb)
        print 'Receiver: '

    def kmeans(self, data, k):
        '''
        Returns centroids 
        '''
        maxIter = 100
        minThreshold = 1.0e-4

        num_samples = len(data)
        centroids = np.random.choice(data, k)
        for i in xrange(maxIter):
            old_centroids = copy.deepcopy(centroids)
            freqs = np.zeros((k,))
            newClusters = np.zeros((k,))
            for sample in xrange(num_samples):
                sampleDiffs = np.zeros((k,))
                for cluster in xrange(k):
                    diff = data[sample]-centroids[cluster]
                    sampleDiffs[cluster] = np.abs(diff)
                minCluster = np.argmin(sampleDiffs)
                freqs[minCluster] += 1
                newClusters[minCluster] += data[sample]
            for cluster in xrange(k):
                if freqs[cluster] > 0:
                    centroids[cluster] = newClusters[cluster] / freqs[cluster]
                else:
                    centroids[cluster] = np.random.choice(data, 1)
            centroid_diff = old_centroids - centroids
            if centroid_diff.dot(centroid_diff) < minThreshold:
                break
        return centroids


    def detect_threshold(self, demod_samples):
        '''
        Returns representative sample values for bit 0, 1 and the threshold.
        Use kmeans clustering with the demodulated samples
        '''
        # fill in your implementation
        clusters = self.kmeans(demod_samples, 2)
        #Cluster assignments are random, so take maxes and mins to get 0s and 1s
        one = max(clusters[0],clusters[1])
        zero = min(clusters[0],clusters[1])
        #Take midpoint to choose threshold
        thresh = (one + zero)*0.5
        return one, zero, thresh

    def detect_energy_offset(self, demod_samples, thresh, one):
        '''
        Detects a rough energy offset for later use in cross-correlation
        '''
        offset = -1
        high_energy_thresh = (thresh + one) * 0.5
        spb_center = self.spb * 0.5
        spb_extent = spb_center * 0.5
        spb_min_bounds = spb_center - spb_extent
        spb_max_bounds = spb_center + spb_extent
        num_samples = len(demod_samples)
        for energy_offset in xrange(num_samples):
            mean = np.mean(demod_samples[energy_offset + spb_min_bounds : energy_offset + spb_max_bounds])
            if mean > high_energy_thresh:
                offset = energy_offset
                break
        return offset

    def detect_cross_correlation_offset(self, offset, demod_samples):
        '''
        Performs normalized cross-correlation to find more
        accurate offset to start of payload data
        '''
        payload_len = len(self.preamble_samples)
        samples = demod_samples[offset : offset + 3 * payload_len]
        num_samples = len(samples)
        pre_offset = -1
        best_off_score = -float('Inf')
        
        for off in xrange(num_samples - payload_len):
            cur_sample = samples[off : off + payload_len]
            nrm = np.sqrt(cur_sample.dot(cur_sample))
            corr = np.sum(cur_sample * self.preamble_samples) / nrm
            if corr > best_off_score:
                best_off_score = corr
                pre_offset = off
        return pre_offset

 
    def detect_preamble(self, demod_samples, thresh, one):
        '''
        Find the sample corresp. to the first reliable bit "1"; this step 
        is crucial to a proper and correct synchronization w/ the xmitter.
        '''

        '''
        First, find the first sample index where you detect energy based on the
        moving average method described in the milestone 2 description.
        '''
        # Fill in your implementation of the high-energy check procedure

        # Find the sample corresp. to the first reliable bit "1"; this step 
        # is crucial to a proper and correct synchronization w/ the xmitter.
        offset = self.detect_energy_offset(demod_samples, thresh, one)
        if offset < 0:
            print '*** ERROR: Could not detect any ones (so no preamble). ***'
            print '\tIncrease volume / turn on mic?'
            print '\tOr is there some other synchronization bug? ***'
            sys.exit(1)

        '''
        Then, starting from the demod_samples[offset], find the sample index where
        the cross-correlation between the signal samples and the preamble 
        samples is the highest. 
        '''
        # Fill in your implementation of the cross-correlation check procedure
        pre_offset = self.detect_cross_correlation_offset(offset, demod_samples)
        '''
        [pre_offset] is the additional amount of offset starting from [offset],
        (not a absolute index reference by [0]). 
        Note that the final return value is [offset + pre_offset]
        '''

        return offset + pre_offset

    def compute_preamble_thresholds(self, demod_preamble_samples):
        '''
        Returns average ones, average zeros, and threshold
        '''
        offset = self.spb * 0.5
        extent = offset * 0.5
        ones = 0
        zeros = 0
        num_ones = np.sum(common.preamble == 1) * offset
        num_zeros = np.sum(common.preamble == 0) * offset
        preamble_len = len(self.preamble_samples)
        preamble_bit_len = len(common.preamble)
        for cur_index in xrange(preamble_bit_len):
            cur_sample_index = cur_index * self.spb + offset
            cur_sample = np.sum(demod_preamble_samples[cur_sample_index - extent : cur_sample_index + extent])
            if self.preamble_samples[cur_sample_index] == 1:
                ones = ones + cur_sample
            else:
                zeros = zeros + cur_sample
        ones = ones / num_ones
        zeros = zeros / num_zeros
        thresh = (ones + zeros) * 0.5
        return ones, zeros, thresh
    
    def demap(self, demod_samples, thresh):
        '''
        Demaps samples into bits using thresh
        to decide what gets mapped to a 0 and 1
        '''
        offset = self.spb * 0.5
        extent = offset * 0.5
        #TODO - Possibly take ceil/floor of payload_bit_len?
        payload_bit_len = len(demod_samples) / self.spb
        payload_bits = []
        for cur_index in xrange(payload_bit_len):
            cur_sample_index = cur_index * self.spb + offset
            #TODO - Possibly check bit boundaries?
            cur_sample = np.mean(demod_samples[cur_sample_index - extent : cur_sample_index + extent])
            if cur_sample >= thresh:
                payload_bits.append(1)
            else:
                payload_bits.append(0)
        return np.array(payload_bits)


        
    def demap_and_check(self, demod_samples, barker_start):
        '''
        Demap the demod_samples (starting from [preamble_start]) into bits.
        1. Calculate the average values of midpoints of each [spb] samples
           and match it with the known preamble bit values.
        2. Use the average values and bit values of the preamble samples from (1)
           to calculate the new [thresh], [one], [zero]
        3. Demap the average values from (1) with the new three values from (2)
        4. Check whether the first [preamble_length] bits of (3) are equal to
           the preamble. If it is proceed, if not print an error message and 
           terminate the program. 
        Output is the array of data_bits (bits without preamble)
        '''
        preamble_len = len(self.preamble_samples)
        demod_preamble_samples = demod_samples[barker_start : barker_start + preamble_len]
        ones, zeros, thresh = self.compute_preamble_thresholds(demod_preamble_samples)
        detected_preamble = self.demap(demod_preamble_samples, thresh)
        print 'Detected preamble was ', detected_preamble, '\t Actual preamble is ', common.preamble
        #Check the demapped preamble to ensure correctness
        if np.sum(detected_preamble != common.preamble) != 0:
            print '*** ERROR: Could not correctly demap preamble!!!!!!! ***\n\n'
            print '\tIs there a bug in the demapping code?'
            sys.exit(1)
        demapped_data_bits = self.demap(demod_samples[barker_start + preamble_len :], thresh)
        return demapped_data_bits

    def demodulate(self, samples):
        '''
        Perform quadrature modulation.
        Return the demodulated samples.
        '''
        # fill in your implementation
        num_samples = len(samples)
        sample_times = np.arange(0,num_samples)
        omega = 2.0 * np.pi * self.fc / (self.samplerate)
        cutoff_frequency = omega / 2.0
        received = np.exp(1j * omega * sample_times) * samples
        lpf_samples = common.lpfilter(received, cutoff_frequency)
        return np.abs(lpf_samples)

    def decode(self, recd_bits):
        return cc.get_databits(recd_bits)
