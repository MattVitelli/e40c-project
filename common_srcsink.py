import numpy
import math
import operator
from collections import Counter
import heapq
import copy

HEADER_MONOTONE = 1
HEADER_TEXT = 2
HEADER_IMAGE = 3

HUFFMAN_CODE_SIZE = 4

NUM_BITS_INT = 32
NUM_BITS_BYTE = 8

# Methods common to both the transmitter and receiver.
def hamming(s1,s2):
    # Given two binary vectors s1 and s2 (possibly of different 
    # lengths), first truncate the longer vector (to equalize 
    # the vector lengths) and then find the hamming distance
    # between the two. Also compute the bit error rate  .
    # BER = (# bits in error)/(# total bits )
    num_elements = min(len(s1),len(s2))
    hamming_d = 0
    for i in xrange(num_elements):
        if s1[i] != s2[i]:
            hamming_d += 1
    ber = float(hamming_d) / float(num_elements)
    return hamming_d, ber

def list_to_string(L):
    return ''.join(str(x) for x in L)

def string_to_list(S):
    return [int(x) for x in S]

def string_to_bits(byte_data):
    # Given byte_data corresponding to a list of bytes,
    # extract the bits and place them in an array
    bits = []
    for byte in bytearray(byte_data):
        for i in xrange(NUM_BITS_BYTE):
            bits.append((byte >> i) & 1)
    return bits

def bits_to_string(bit_data):
    # Given bit_data corresponding to a list of bits,
    # extract the bytes and place them in an array
    byte_array = []
    num_bytes = len(bit_data) / NUM_BITS_BYTE
    for j in xrange(num_bytes):
        byte = 0
        stride = j * NUM_BITS_BYTE
        for i in xrange(NUM_BITS_BYTE):
            byte = byte | (bit_data[stride + i] << i)
        byte_array.append(chr(byte))
    msg = list_to_string(byte_array)
    return msg

def int_to_bits(num):
    # Given byte_data corresponding to a list of bytes,
    # extract the bits and place them in an array
    bits = []
    for i in xrange(NUM_BITS_INT):
        bits.append((num >> i) & 1)
    return bits

def bits_to_int(bit_data):
    # Given bit_data corresponding to a list of bits,
    # extract the bytes and place them in an array
    num = 0
    for i in xrange(NUM_BITS_INT):
        num = num | (int(bit_data[i]) << i)
    return num

class HuffmanNode:
    def __init__(self):
        self.left = None
        self.right = None
        self.value = None

class HuffmanTree:
    def __init__(self):
        self.root = None;
        self.symbol_table = []
        self.words_to_symbols = []

    def encode(self, srcbits):
        # Build frequencies
        word_frequencies = Counter()
        num_words = len(srcbits) / HUFFMAN_CODE_SIZE
        for i in xrange(num_words):
            stride = i * HUFFMAN_CODE_SIZE
            word = list_to_string(srcbits[stride : stride + HUFFMAN_CODE_SIZE])
            word_frequencies[word]+=1
        
        # Build huffman tree
        heap = []
        for symbol in word_frequencies.keys():
            leaf = HuffmanNode()
            leaf.value = symbol
            heapq.heappush(heap, (word_frequencies[symbol], leaf))
        while len(heap) > 1:
            priority_a, node_a = heapq.heappop(heap)
            priority_b, node_b = heapq.heappop(heap)
            parent = HuffmanNode()
            parent.left = node_a
            parent.right = node_b
            priority = priority_a + priority_b
            heapq.heappush(heap, (priority, parent))
        total_priority, self.root = heapq.heappop(heap)
            
        self.symbol_table = dict()
        self.words_to_symbols = dict()
        initial_symbol = []
        self.recursively_build_symbols(self.root, initial_symbol)
        
        compressed = []
        for i in xrange(num_words):
            stride = i * HUFFMAN_CODE_SIZE
            word = list_to_string(srcbits[stride : stride + HUFFMAN_CODE_SIZE])
            compressed.extend(string_to_list(self.words_to_symbols[word]))
        return self.symbol_table, compressed

    def recursively_build_symbols(self, node, symbol):
        if node.left == None and node.right == None:
            cur_symbol = list_to_string(symbol)
            self.symbol_table[cur_symbol] = node.value
            self.words_to_symbols[node.value] = cur_symbol
        else:
            symbol.append(0)
            self.recursively_build_symbols(node.left, symbol)
            symbol.pop()
            symbol.append(1)
            self.recursively_build_symbols(node.right, symbol)
            symbol.pop()
