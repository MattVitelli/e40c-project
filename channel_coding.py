'''
Channel coding module for transmitter and receiver
'''

import numpy

import hamming_db   # Matrices for Hamming codes

NUM_BITS_INT = 32
HEADER_LENGTH = 3
HEADER_INDEX = 0
NUM_HEADER_BITS = 2 + NUM_BITS_INT

BITS_FOR_INDEX = [numpy.array([0,0]),
                  numpy.array([0,1]),
                  numpy.array([1,0]),
                  numpy.array([1,1])]

def int_to_bits(num):
# Given byte_data corresponding to a list of bytes,
# extract the bits and place them in an array
    bits = []
    for i in xrange(NUM_BITS_INT):
        bits.append((num >> i) & 1)
    return bits

def bits_to_int(bit_data):
# Given bit_data corresponding to a list of bits,
# extract the bytes and place them in an array
    num = 0
    for i in xrange(NUM_BITS_INT):
        num = num | (int(bit_data[i]) << i)
    return num

def encode_with_generator(K, databits, G):
    code = None
    num_padded_bits = len(databits) % K
    if num_padded_bits > 0:
        padding = numpy.zeros((num_padded_bits,))
        databits = numpy.concatenate((databits, padding), 1)

    print "Encode w/ Generator ", K, " ", G

    for i in xrange(len(databits)/K):
        stride = i*K
        cur_databits = databits[stride : stride+K]
        cur_code = numpy.dot(cur_databits, G)
        cur_code %= 2
        if code is None:
            code = cur_code
        else:
            code = numpy.concatenate((code, cur_code), 1)
    return code

def decode_with_parity(N, K, encoded_bits, H):
    decoded = None
    NO_ERROR = numpy.zeros((N-K,))
    for i in xrange(len(encoded_bits)/N):
        stride = i * N
        cur_code = encoded_bits[stride : stride+N].T
        syndrome = numpy.dot(H, cur_code)
        syndrome %= 2
        #Check if syndrome is non-zero (i.e. if an error occurred)
        if numpy.any(syndrome != NO_ERROR):
            error_col = -1
            for cur_col in xrange(N):
                if numpy.all(H[:,cur_col] == syndrome):
                    error_col = cur_col
                    break
            if error_col < 0:
                raise Exception("Error - unknown syndrome (possibly multiple bit errors?)")
            else:
                if cur_code[error_col] == 1:
                   cur_code[error_col] = 0
                else:
                   cur_code[error_col] = 1
        if decoded is None:
            decoded = cur_code[0:K]
        else:
            decoded = numpy.concatenate((decoded, cur_code[0:K]),1)

    return decoded

''' Transmitter side ---------------------------------------------------
'''
def get_frame(databits, cc_len):
    '''
    Perform channel coding to <databits> with Hamming code of n=<cc_len>
    Add header and also apply Hamming code of n=3
    Return the resulted frame (channel-coded header and databits)
    '''
    n, k, idx, G = hamming_db.gen_lookup(cc_len)

    databits_array = numpy.array(databits)
    print "SOURCE DATABITS BEGIN"
    print databits_array
    print "SOURCE DATABITS END"

    #Create and encode header
    header = get_header(databits_array, idx)
    nH, kH, idxH, GH = hamming_db.gen_lookup(HEADER_LENGTH)
    coded_header = encode_with_generator(kH, header, GH)

    #Encode data
    coded_data = encode_with_generator(k, databits_array, G)
    """
    num_padded_bits = len(coded_data) % n
    if num_padded_bits > 0:
        padding = numpy.zeros((num_padded_bits,))
        coded_data = numpy.concatenate((coded_data, padding), 1)
    """
    #Concatenate into frame
    frame = numpy.concatenate((coded_header, coded_data), 1)
    print "CODED HEADER SIZE IS ", len(coded_header)
    print "CODED DATA SIZE IS ", len(coded_data)
    print "FULL FRAME SIZE IS ", len(frame)
    print "coded frame is ", coded_data

#    import pdb; pdb.set_trace()
    return frame
    
def get_header(payload, index):
    '''
    Construct and return header for channel coding information.
    Do not confuse this with header from source module.
    Communication system use layers and attach nested headers from each layers 
    '''
    payload_size = numpy.array(int_to_bits(len(payload)))
    print 'PAYLOAD SIZE IS ', len(payload)
    header = numpy.concatenate((BITS_FOR_INDEX[index], payload_size), 1)
    print 'HEADER SIZE IS ', len(header)
    return header
    
def encode(databits, cc_len):
    '''
    Perfrom channel coding to <databits> with Hamming code of n=<cc_len>
    Pad zeros to the databits if needed.
    Return the index of our used code and the channel-coded databits
    '''
    n, k, index, G = hamming_db.gen_lookup(cc_len)
    coded_bits = get_frame(databits, cc_len)
   
    return index, coded_bits

''' Receiver side ---------------------------------------------------
'''    
def get_databits(recd_bits):
    '''
    Return channel-decoded data bits
    Parse the header and perform channel decoding.
    Note that header is also channel-coded    
    '''
    #Decode header
    nH, kH, HH = hamming_db.parity_lookup(HEADER_INDEX)
    header_bits = decode_with_parity(nH, kH, recd_bits[0:NUM_HEADER_BITS*nH], HH)
    payload_size = bits_to_int(header_bits[2:])

    print "DECODING HEADER SIZE IS ", len(header_bits)
    print "PAYLOAD SIZE IS ", payload_size

    encoded_index = -1
    for i in xrange(len(BITS_FOR_INDEX)):
        if numpy.all(BITS_FOR_INDEX[i] == header_bits[0:2]):
            encoded_index = i
            break
    if encoded_index == -1:
        raise Exception("Error - Could not resolve header index")



    n, k, H = hamming_db.parity_lookup(encoded_index)
    stride = NUM_HEADER_BITS * nH
    length = payload_size * n

    print "DECODING - RECOVERED INDEX IS ", encoded_index
    print "MESSAGE LENGTH IS ", len(recd_bits[stride:stride+length])
    print "TOTAL MESSAGE LENGTH IS ", len(recd_bits)
    print "RECEIVED BITS ", recd_bits[stride:stride+length]

    databits_array = decode_with_parity(n, k, recd_bits[stride : stride + length], H)
    databits_array = databits_array[0:payload_size]
    print "DATABITS SIZE IS ", len(databits_array)

    print "DEST DATABITS BEGIN"
    print databits_array
    print "DEST DATABITS END"

    return list(databits_array)

def decode(coded_bits, index):
    '''
    Decode <coded_bits> with Hamming code which corresponds to <index>
    Return decoded bits
    '''
    decoded_bits = get_databits(coded_bits)

    return decoded_bits
