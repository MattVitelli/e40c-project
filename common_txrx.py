import numpy
import math
import operator

import binascii
# Methods common to both the transmitter and receiver.

preamble = numpy.array([1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1])

def get_preamble_samples(spb):
    return numpy.repeat(preamble, spb)

def lpfilter(samples_in, omega_cut):
    '''
    A low-pass filter of frequency omega_cut.
    '''
    # set the filter unit sample response
    # convolve unit sample response with input samples
    # fill in your implementation
    L = 50
    n = numpy.arange(-L,L+1)
    zero_idx = L
    n[zero_idx] = 1 # Avoid division by 0

    h = numpy.sin(omega_cut * n) / (numpy.pi * n)
    h[zero_idx] = omega_cut / numpy.pi

    num_samples = len(samples_in)
    samples_out = numpy.zeros((num_samples,))
    #Flip for convolution
    h = h[::-1]
    for cur_samp in xrange(num_samples):
        min_idx_s = max(0, cur_samp-L)
        max_idx_s = min(num_samples, cur_samp+L)
        min_idx_h = zero_idx-(cur_samp - min_idx_s)
        max_idx_h = zero_idx+(max_idx_s - cur_samp)
        samples_out[cur_samp] = h[min_idx_h : max_idx_h].dot(samples_in[min_idx_s : max_idx_s])
    return samples_out

